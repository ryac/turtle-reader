$(document).foundation();

$(function() {
  var source, template;
  source = $('#triple-row').html();
  template = Handlebars.compile(source);
  return $('a.button').on('click', function(e) {
    var html, url;
    e.preventDefault();
    url = $('.field-url').val();
    html = '';
    if (url !== '') {
      $('.ttl-loading').show();
      $('.ttl-result').hide();
      return $.get('/parse_ttl', {
        ttl_url: url
      }, function(data) {
        $.each(data, function(index, value) {
          return html += template(value);
        });
        $('.result tbody').html(html);
        $('.ttl-loading').hide();
        return $('.ttl-result').show();
      });
    }
  });
});
