# Foundation JavaScript - documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation()

$ ->

	source = $('#triple-row').html()
	template = Handlebars.compile source

	$('a.button').on 'click', (e) ->
		e.preventDefault()
		
		url = $('.field-url').val()
		html = ''

		if (url isnt '')

			$('.ttl-loading').show()
			$('.ttl-result').hide()

			$.get '/parse_ttl', { ttl_url: url }, (data) ->

				$.each data, (index, value) ->
					html += template value

				$('.result tbody').html html

				$('.ttl-loading').hide()
				$('.ttl-result').show()
