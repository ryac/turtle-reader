express = require 'express'
request = require 'request'
n3 = require 'n3'
router = express.Router()

# GET home page..
router.get '/', (req, res) ->
	res.render 'index'

router.get '/parse_ttl', (req, res) ->
	parser = n3.Parser()
	data = []
	request req.query.ttl_url, (err, response, body) ->
		if (not err and response.statusCode is 200)
			parser.parse body, (error, triple, prefixes) ->
				if (triple)
					trip =
						subject: triple.subject
						predicate: triple.predicate
						object: triple.object
					data.push trip
				else
					res.send data

module.exports = router